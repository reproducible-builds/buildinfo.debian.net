from django.conf.urls import url

from . import views

app_name = 'static'

urlpatterns = (url(r'^$', views.landing, name='landing'),)
