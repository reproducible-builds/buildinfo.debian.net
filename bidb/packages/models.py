from django.db import models
from django.urls import reverse
from django.utils import timezone


class Source(models.Model):
    name = models.CharField(max_length=200, unique=True)

    created = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ('name',)
        get_latest_by = 'created'

    def __str__(self):
        return "pk={} name={!r}".format(self.pk, self.name)

    def get_absolute_url(self):
        return reverse('packages:source', args=(self.name,))


class Binary(models.Model):
    name = models.CharField(max_length=200, unique=True)

    class Meta:
        ordering = ('name',)
        get_latest_by = 'created'

    def __str__(self):
        return "pk={} name={!r}".format(self.pk, self.name)

    def get_absolute_url(self):
        return reverse('packages:binary', args=(self.name,))


class Architecture(models.Model):
    name = models.CharField(max_length=200, unique=True)

    created = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ('name',)
        get_latest_by = 'created'

    def __str__(self):
        return "pk={} name={!r}".format(self.pk, self.name)
