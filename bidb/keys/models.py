from django.db import models, transaction
from django.utils import timezone


class Key(models.Model):
    uid = models.CharField(max_length=255, unique=True)

    name = models.TextField()

    created = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ('-created',)
        get_latest_by = 'created'

    def __str__(self):
        return "pk={} uid={!r} name={!r}".format(self.pk, self.uid, self.name)

    def save(self, *args, **kwargs):
        created = not self.pk

        super(Key, self).save(*args, **kwargs)

        if created:
            from .tasks import update_or_create_key

            transaction.on_commit(lambda: update_or_create_key.delay(self.uid))
