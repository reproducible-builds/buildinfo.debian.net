INSTALLED_APPS = (
    'django.contrib.contenttypes',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'djcelery',
    'template_tests',
    'bidb.api',
    'bidb.keys',
    'bidb.packages',
    'bidb.buildinfo',
    'bidb.buildinfo.buildinfo_submissions',
    'bidb.static',
    'bidb.utils',
)
