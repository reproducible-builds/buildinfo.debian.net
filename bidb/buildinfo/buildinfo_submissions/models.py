import functools

from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.crypto import get_random_string


class Submission(models.Model):
    buildinfo = models.ForeignKey(
        'buildinfo.Buildinfo',
        related_name='submissions',
        on_delete=models.CASCADE,
    )

    slug = models.CharField(
        unique=True,
        default=functools.partial(
            get_random_string, 8, '3479abcdefghijkmnopqrstuvwxyz'
        ),
        max_length=8,
    )

    key = models.ForeignKey(
        'keys.Key', related_name='submissions', on_delete=models.CASCADE
    )

    created = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ('created',)
        get_latest_by = 'created'

    def __str__(self):
        return "pk={} buildinfo={!r}".format(self.pk, self.buildinfo)

    def get_absolute_url(self):
        return reverse(
            'buildinfo:submissions:view',
            args=(
                self.buildinfo.sha1,
                self.buildinfo.get_filename(),
                self.slug,
            ),
        )

    def get_storage_name(self):
        return 'buildinfo_submissions.Submission/{}/{}/{}'.format(
            self.slug[:2], self.slug[2:4], self.slug
        )
