from django.db import models
from django.urls import reverse
from django.utils import timezone


class Buildinfo(models.Model):
    sha1 = models.CharField(max_length=40, unique=True)

    source = models.ForeignKey(
        'packages.Source', related_name='buildinfos', on_delete=models.CASCADE
    )

    architecture = models.ForeignKey(
        'packages.Architecture',
        related_name='buildinfos',
        on_delete=models.CASCADE,
    )
    version = models.CharField(max_length=200)

    build_path = models.CharField(max_length=512)
    build_date = models.DateTimeField(null=True)
    build_origin = models.ForeignKey(
        'Origin', null=True, on_delete=models.CASCADE
    )
    build_architecture = models.ForeignKey(
        'packages.Architecture',
        related_name='buildinfos_build',
        on_delete=models.CASCADE,
    )

    environment = models.TextField()

    created = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ('-created',)
        get_latest_by = 'created'

    def __str__(self):
        return u"pk=%d source=%r version=%r" % (
            self.pk,
            self.source,
            self.version,
        )

    def get_absolute_url(self):
        return reverse('buildinfo:view', args=(self.sha1, self.get_filename()))

    def get_absolute_raw_url(self):
        return reverse(
            'buildinfo:raw-text', args=(self.sha1, self.get_filename())
        )

    def get_filename(self):
        return '{}_{}_{}'.format(
            self.source.name,
            self.version,
            self.architecture.name.split(' ')[0],
        )

    def get_storage_name(self):
        return 'buildinfo.Buildinfo/{}/{}/{}'.format(
            self.sha1[:2], self.sha1[2:4], self.sha1
        )


class Binary(models.Model):
    buildinfo = models.ForeignKey(
        Buildinfo, related_name='binaries', on_delete=models.CASCADE
    )

    binary = models.ForeignKey(
        'packages.Binary',
        related_name='generated_binaries',
        on_delete=models.CASCADE,
    )

    created = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ('binary__name',)
        get_latest_by = 'created'
        unique_together = (('buildinfo', 'binary'),)

    def __str__(self):
        return u"pk=%d binary=%r" % (self.pk, self.binary)


class Checksum(models.Model):
    """
    Not the same as Binary as we could potentially have a Checksum for a
    Binary, etc.
    """

    buildinfo = models.ForeignKey(
        Buildinfo, related_name='checksums', on_delete=models.CASCADE
    )

    filename = models.CharField(max_length=255)
    size = models.IntegerField()

    checksum_md5 = models.CharField(max_length=100)
    checksum_sha1 = models.CharField(max_length=100)
    checksum_sha256 = models.CharField(max_length=100)

    binary = models.OneToOneField(
        Binary, null=True, related_name='checksum', on_delete=models.CASCADE
    )

    created = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ('-created',)
        get_latest_by = 'created'
        unique_together = (('buildinfo', 'filename'),)

    def __str__(self):
        return "pk={} filename={!r}".format(self.pk, self.filename)


class Origin(models.Model):
    name = models.CharField(max_length=255)

    created = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ('name',)
        get_latest_by = 'created'

    def __str__(self):
        return "pk={} name=%r".format(self.pk, self.name)
