import contextlib

from django.core.cache import cache


# Not all cache backends support .lock()
if hasattr(cache, 'lock'):
    def lock(*args, **kwargs):
        return cache.lock(*args, **kwargs)
else:
    @contextlib.contextmanager
    def lock(*args, **kwargs):
        yield
